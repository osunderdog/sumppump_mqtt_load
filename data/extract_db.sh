#! /bin/sh
##
## Quick script to extract data from sqlite3 database and
## drop it as a set of inserts to local directory
## zip it up to give it that nice slim look
##
DBFN=./sumppump.db3
FN=`tempfile --prefix=evt_ --suffix=.sql  --directory .`

echo ".output $FN\n.dump event\n.quit" | sqlite3 $DBFN

gzip $FN

echo 'Done.'
