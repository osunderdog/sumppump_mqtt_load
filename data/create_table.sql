

create table if not exists
event
(
	dt datetime default current_timestamp,
	type varchar(30),
	value integer
);
