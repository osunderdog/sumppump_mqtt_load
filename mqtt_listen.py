import configparser
import paho.mqtt.client as mqtt
import sqlite3

##Security configuration credentials.
##expect to have section: 'MQTT INFO' containing username and password entries.
secConfFN = "./security/credentials.ini"
secConf = configparser.RawConfigParser()
secConf.read(secConfFN)
username = secConf['MQTT CREDENTIALS']['username']
password = secConf['MQTT CREDENTIALS']['password']

##MQTT Configuration information
appConfFN = "./config.ini"
appConf = configparser.RawConfigParser()
appConf.read(appConfFN)

dbfile = appConf['DB CONFIG']['dbfile']
volt_subject = appConf['MQTT INFO']['volt_subject']
pump_subject = appConf['MQTT INFO']['pump_subject']


##MAIN
print("username:[{}]".format(username))
print("password:[{}]".format(password))
print("dbfile:[{}]".format(dbfile))

conn = sqlite3.connect(dbfile)
c = conn.cursor()

last_volt_value = 0;
last_volt_count = 0;

last_pump_value = 0;

    # add a date time to each received data.
    # only count cases where value is the same as last.
    # record results to a database. 
    
def on_volt_message(client, userdata, msg):
    global last_volt_value
    global last_volt_count
    last_volt_count += 1
    value = int(msg.payload)

    #well this sucks, but if I don't govern a little, the data is very dense and not that useful...
    
    if((value > 30) or (last_volt_count > 1000)):
        last_volt_count = 0
        try:
            c.execute("insert into event (type, value) VALUES (?,?)",
                      ('VOLT', value))
        except sqlite3.IntegrityError:
            print('Nope')
            
        conn.commit()
        last_volt_value = value

def on_pump_message(client, userdata, msg):
    global last_pump_value
    value = int(msg.payload)
    if(value != last_pump_value):
        try:
            c.execute("insert into event (type, value) VALUES (?,?)",
                      ('PUMP', value))
        except sqlite3.IntegrityError:
            print('Nope')
            
        conn.commit()
        last_pump_value = value

def on_unknown_message(client, userdata, msg):
    print("huh:[{}]").format(msg.payload)
        
client = mqtt.Client()
client.username_pw_set(username, password);

print("callback added for [{}]".format(volt_subject))
client.message_callback_add(volt_subject, on_volt_message)

print("callback added for [{}]".format(pump_subject))
client.message_callback_add(pump_subject, on_pump_message)
client.on_message = on_unknown_message

client.connect("localhost", 1883, 60)
print("Connected...")
client.subscribe("osunderdog/feeds/#")

client.loop_forever()



