#! /bin/sh
DBFN='../data/sumppump.db3'
VOLT_TFN=`tempfile`
echo "dt|value" > $VOLT_TFN
echo  "select datetime(dt, 'localtime') as dt, value from event where type = 'VOLT';" \
    | sqlite3 $DBFN >> $VOLT_TFN

PUMP_TFN=`tempfile`
echo "dt|value" > $PUMP_TFN
echo  "select datetime(dt, 'localtime') as dt, value from event where type = 'PUMP';" \
    | sqlite3 $DBFN >> $PUMP_TFN

gnuplot -e "vfn='$VOLT_TFN'; pfn='$PUMP_TFN'; load 'pump_plot.gp'"

rm $VOLT_TFN
rm $PUMP_TFN
