##gnuplot view pump results from database.
##
set datafile separator "|"
set xdata time
set timefmt "%Y-%m-%d %H:%M:%S"
set format x "%H:%M:%S"
set yrange [0:600]
set style data points
pumpscale(x) = (500*(x))
plot vfn using "dt":"value" t 'voltage', \
     pfn using "dt":(pumpscale($2)) t 'pump' w impulses

pause -1